package pl.lesniewicz.webshopapp.webshopapp.service;

import org.springframework.stereotype.Service;
import pl.lesniewicz.webshopapp.webshopapp.data.Product;
import pl.lesniewicz.webshopapp.webshopapp.data.repository.ProductRepository;
import pl.lesniewicz.webshopapp.webshopapp.exception.ProductNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product>findAllProduct(){
        return productRepository.findAll();
    }

    public Product findById(Long id){
        Optional <Product> meybeProduct = productRepository.findById(id);
        if (meybeProduct.isPresent()){
            Product product = meybeProduct.get();
            return product;
        }else throw new ProductNotFoundException("A product with a padded ID does not exist.");
    }
}
