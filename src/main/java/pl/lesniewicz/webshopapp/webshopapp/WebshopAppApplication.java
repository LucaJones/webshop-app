package pl.lesniewicz.webshopapp.webshopapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebshopAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebshopAppApplication.class, args);
	}

}
