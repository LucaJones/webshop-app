package pl.lesniewicz.webshopapp.webshopapp.data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

@Entity
@Data

public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String description;
    private List<String> categories;
    private long price;

    public Product(){}

    public Product(String name, String description, List<String> categories, long price) {
        this.name = name;
        this.description = description;
        this.categories = categories;
        this.price = price;
    }
}
