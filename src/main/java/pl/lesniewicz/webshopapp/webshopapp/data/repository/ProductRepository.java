package pl.lesniewicz.webshopapp.webshopapp.data.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.lesniewicz.webshopapp.webshopapp.data.Product;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    @Query("SELECT r FROM Product r")
    List<Product> findAll();

}
